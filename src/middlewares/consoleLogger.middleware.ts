import { Request, Response, NextFunction } from 'express';

const loggerMiddleware = (req: Request, res: Response, next: NextFunction) => {
	console.log(
		`REQUEST: ${req.hostname} ${req.method} ${req.path} \n ${JSON.stringify(
			req.body,
		)} \n `,
	);
	next();
};

export default loggerMiddleware;
