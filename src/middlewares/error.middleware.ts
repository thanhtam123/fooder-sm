import ApiError from '../utils/ApiError';
import HttpStatus from '../utils/httpStatusCode';
import { NextFunction, Request, Response } from 'express';

// conver error to ApiError
export const errorConverter = async (
	err: any,
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	let apiErr = err;
	if (!(apiErr instanceof ApiError)) {
		const statusCode = apiErr.statusCode /*|| apiErr.name === 'MongoError'*/
			? HttpStatus.BAD_REQUEST
			: HttpStatus.INTERNAL_SERVER_ERROR;
		const message = apiErr.message || HttpStatus[statusCode];
		apiErr = new ApiError(statusCode, message, false, err.stack);
	}
	next(apiErr);
};

// handle error from catchAsync
export const errorHandler = async (
	apiErr: ApiError,
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	let { statusCode, message, stack } = apiErr;
	const response = {
		code: statusCode,
		message,
		stack: stack,
	};
	res.status(statusCode).send(response);
};
