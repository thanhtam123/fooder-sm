import { ConnectionOptions } from 'typeorm';

export const ormConfig: ConnectionOptions = {
	type: 'mysql',
	host: 'localhost',
	port: 3306,
	username: 'root',
	password: 'thanhtam123',
	database: `fooder`,
	entities: ['src/entities/*.entity{.ts,.js}'],
	// logging: true,
	synchronize: true,
	// "migrations": ["src/migration/*.ts"],
	// "subscribers": ["src/subscriber/*.ts"],
	// "cli": {
	// 	"entitiesDir": "src/entity",
	// 	"migrationsDir": "src/migration",
	// 	"subscribersDir": "src/subscriber"
	// }
};

export const getOrmConfig = (): ConnectionOptions => {
	return {
		type: 'mysql',
		host: `${process.env.DB_HOST}`,
		port: Number(process.env.DB_PORT),
		username: `${process.env.DB_USER}`,
		password: `${process.env.DB_PASSWORD}`,
		database: `${process.env.DB_DB}`,
		entities: ['src/entities/*.entity{.ts,.js}'],
		// logging: true,
		synchronize: true,
		// "migrations": ["src/migration/*.ts"],
		// "subscribers": ["src/subscriber/*.ts"],
		// "cli": {
		// 	"entitiesDir": "src/entity",
		// 	"migrationsDir": "src/migration",
		// 	"subscribersDir": "src/subscriber"
		// }
	};
};
