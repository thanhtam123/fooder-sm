export const getMongUri = () => {
	return process.env.MONGO_URI;
};

export const getMongoConfig = () => {
	return {
		useNewUrlParser: true, // error in @types/mongoose package
		useUnifiedTopology: true,
		useCreateIndex: true,
		useFindAndModify: false,
	};
};
