import { Request, Response, NextFunction } from 'express';
import HttpStatus from './httpStatusCode';

interface optionsInterface {
	statusCode?: number;
}

const catchAsync = (fn: Function, opts: optionsInterface = {}) => {
	const statusCode: number = opts.statusCode
		? opts.statusCode
		: HttpStatus.OK;
	return (req: Request, res: Response, next: NextFunction) => {
		Promise.resolve(fn(req, res, next))
			.then((result: object) => {
				res.status(statusCode).send(result);
			})
			.catch((err) => next(err));
	};
};

export default catchAsync;
