import { cleanEnv, port, str } from 'envalid';

const validateEnv = () => {
	cleanEnv(process.env, {
		PORT: port(),

		DB_HOST: str(),
		DB_PORT: port(),
		DB_USER: str(),
		DB_PASSWORD: str(),
		DB_DB: str(),

		MONGO_URI: str(),
	});
};

export default validateEnv;
