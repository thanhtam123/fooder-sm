import App from './app';
import 'dotenv/config';
import controllers from './api';
import validateEnv from './utils/validateEnv';

validateEnv();

const app = new App(controllers);
app.listen();
