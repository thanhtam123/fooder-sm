import ApiError from '../utils/ApiError';
import HttpStatus from '../utils/httpStatusCode';

export default class HttpBadRequestException extends ApiError {
	constructor(message: string) {
		super(HttpStatus.BAD_REQUEST, message);
	}
}
