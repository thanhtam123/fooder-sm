import ApiError from '../utils/ApiError';
import HttpStatus from '../utils/httpStatusCode';

export default class HttpUnauthorizedException extends ApiError {
	constructor(message: string) {
		super(HttpStatus.UNAUTHORIZED, message);
	}
}
