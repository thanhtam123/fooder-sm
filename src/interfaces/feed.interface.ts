export default interface FeedInterface {
	_id?: string;
	userId: string;
	content: string;
	imgs?: string;

	createdAt?: string;
	updatedAt?: string;
}
