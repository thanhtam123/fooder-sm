import { IsNotEmpty, IsOptional } from 'class-validator';

export default class CreateFeedDto {
	@IsOptional()
	userId?: string;

	@IsNotEmpty()
	content: string;
}
