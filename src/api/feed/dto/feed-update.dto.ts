import { IsOptional } from 'class-validator';

export default class UpdateFeedDto {
	@IsOptional()
	content?: string;
}
