export default class FeedDto {
	_id?: string;
	userId: string;
	content: string;
	imgs?: string[];

	createdAt?: Date;
	updatedAt?: Date;
}
