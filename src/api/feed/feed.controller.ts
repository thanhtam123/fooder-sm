import BaseController from '../../base/base.controller';
import FeedService from './feed.service';
import HttpStatus from '../../utils/httpStatusCode';
import catchAsync from '../../utils/catchAsync';
import { Request, Response, NextFunction } from 'express';
import validationMiddleware from '../../middlewares/validation.middleware';
import CreateFeedDto from './dto/feed-create.dto';
import UpdateFeedDto from './dto/feed-update.dto';

export default class FeedController extends BaseController {
	private feedService: FeedService;

	constructor(feedService: FeedService) {
		super('/feed');
		this.feedService = feedService;

		this.intializeRoutes();
	}

	intializeRoutes() {
		this.router.route('/').get(this.findAll);
		this.router
			.route('/user/:id')
			.post(validationMiddleware(CreateFeedDto, true), this.create);
		this.router
			.route('/:id')
			.get(this.findById)
			.put(validationMiddleware(UpdateFeedDto, true), this.updateById);
	}

	private create = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const feed = await this.feedService.create({
				...req.body,
				userId: req.params.id,
			});
			return { feed };
		},
		{ statusCode: HttpStatus.CREATED },
	);

	private findById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const feed = await this.feedService.findById(req.params.id);
			return { feed };
		},
	);

	private findAll = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const feeds = await this.feedService.findAll();
			return { feeds };
		},
	);

	private updateById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const feed = await this.feedService.updateById(
				req.params.id,
				req.body,
			);
			return { feed };
		},
	);
}
