import FeedInterface from '../../interfaces/feed.interface';
import CRUDBaseInterface from '../../base/crud-base.interface';
import CreateFeedDto from './dto/feed-create.dto';
import UpdateFeedDto from './dto/feed-update.dto';
import FeedModel from '../../models/feed.model';
import HttpNotFoundException from '../../http-exceptions/httpNotFound.exception';

export default class FeedService implements CRUDBaseInterface {
	async create(createBody: CreateFeedDto): Promise<FeedInterface> {
		const feed = await FeedModel.create(createBody);
		return feed;
	}

	async findById(id: string): Promise<FeedInterface> {
		const feed = await FeedModel.findById(id);
		if (!feed) {
			throw new HttpNotFoundException(`Not found feed with id ${id}`);
		}
		return feed;
	}

	async findByUser(userId: string): Promise<FeedInterface[]> {
		const feeds = await FeedModel.find({ userId: userId });
		return feeds;
	}

	async findAll(): Promise<FeedInterface[]> {
		return FeedModel.find({});
	}

	async updateById(
		id: string,
		updateBody: UpdateFeedDto,
	): Promise<FeedInterface> {
		const feed = await FeedModel.findById(id);
		if (!feed) {
			throw new HttpNotFoundException(`Not found feed with id ${id}`);
		}

		Object.assign(feed, updateBody);
		await feed.save();
		return feed;
	}

	async deleteById(id: string): Promise<any> {}
}
