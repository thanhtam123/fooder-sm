import BaseController from '../base/base.controller';
import AuthController from './auth/auth.controller';
import AuthService from './auth/auth.service';
import FeedController from './feed/feed.controller';
import FeedService from './feed/feed.service';
import UserController from './user/user.controller';
import UserService from './user/user.service';

const userService = new UserService();
const authService = new AuthService();
const feedService = new FeedService();

const userController = new UserController(userService);
const authController = new AuthController(authService);
const feedController = new FeedController(feedService);

const controllers: BaseController[] = [
	userController,
	authController,
	feedController,
];

export default controllers;
