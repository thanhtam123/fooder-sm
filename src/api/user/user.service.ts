import HttpNotFoundException from '../../http-exceptions/httpNotFound.exception';
import CRUDBaseInterface from '../../base/crud-base.interface';
import UserEntity from '../../entities/user.entity';
import CreateUserDto from './dto/user-create.dto';
import UpdateUserDto from './dto/user-update.dto';
import UserInterface from '../../interfaces/user.interface';
import HttpBadRequestException from '../../http-exceptions/httpBadRequest.exception';

export default class UserService implements CRUDBaseInterface {
	async create(createBody: CreateUserDto): Promise<UserInterface> {
		const { username, email } = createBody;
		const nameUser = await UserEntity.findOne({ username });
		if (nameUser) {
			throw new HttpBadRequestException(
				`Username '${username}' is already in use.`,
			);
		}
		const emailUser = await UserEntity.findOne({ email });
		if (emailUser) {
			throw new HttpBadRequestException(
				`Email '${email}' is already in use.`,
			);
		}

		const user = UserEntity.create(createBody);
		await UserEntity.save(user);
		return user;
	}

	async findById(id: string): Promise<UserInterface> {
		const user = await UserEntity.findOne(id);
		if (!user) {
			throw new HttpNotFoundException(`Not found user with id ${id}`);
		}
		return user;
	}

	async findAll(): Promise<UserInterface[]> {
		const users = await UserEntity.find();
		return users;
	}

	async updateById(
		id: string,
		updateBody: UpdateUserDto,
	): Promise<UserInterface> {
		const { username, email } = updateBody;
		if (username) {
			const nameUser = await UserEntity.findOne({ username });
			if (nameUser && nameUser.id != id) {
				throw new HttpBadRequestException(
					`Username '${username}' is already in use.`,
				);
			}
		}
		if (email) {
			const emailUser = await UserEntity.findOne({ email });
			if (emailUser && emailUser.id != id) {
				throw new HttpBadRequestException(
					`Email '${email}' is already in use.`,
				);
			}
		}

		const user = await UserEntity.findOne(id);
		if (!user) {
			throw new HttpNotFoundException(`Not found user with id ${id}`);
		}

		Object.assign(user, updateBody);
		await user.save();

		return user;
	}

	async deleteById(id: string): Promise<any> {}
}
