import { IsEmail, IsOptional, IsString, MinLength } from 'class-validator';

export default class UpdateUserDto {
	@IsOptional()
	id?: string;

	@IsOptional()
	username?: string;

	@IsOptional()
	@IsEmail()
	email?: string;

	@IsOptional()
	@IsString()
	@MinLength(8)
	password?: string;
}
