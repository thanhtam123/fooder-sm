import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export default class CreateUserDto {
	@IsNotEmpty()
	username: string;

	@IsNotEmpty()
	@IsEmail()
	email: string;

	@IsNotEmpty()
	@IsString()
	@MinLength(8)
	password: string;
}
