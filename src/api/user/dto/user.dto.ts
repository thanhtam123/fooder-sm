export default class UserDto {
	id: string;
	username: string;
	email: string;
	password: string;
	createdAt?: Date;
	updatedAt?: Date;
}
