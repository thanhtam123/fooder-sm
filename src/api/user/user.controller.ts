import BaseController from '../../base/base.controller';
import HttpStatus from '../../utils/httpStatusCode';
import catchAsync from '../../utils/catchAsync';
import UserService from './user.service';
import { Request, Response, NextFunction } from 'express';
import validationMiddleware from '../../middlewares/validation.middleware';
import CreateUserDto from './dto/user-create.dto';
import UpdateUserDto from './dto/user-update.dto';

export default class UserController extends BaseController {
	private userService: UserService;

	constructor(userService: UserService) {
		super('/user');
		this.userService = userService;

		this.intializeRoutes();
	}

	intializeRoutes() {
		this.router
			.route('/')
			.get(this.findAll)
			.post(validationMiddleware(CreateUserDto), this.create);
		this.router
			.route('/:id')
			.get(this.findById)
			.put(validationMiddleware(UpdateUserDto, true), this.updateById);
	}

	private create = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const user = await this.userService.create(req.body);
			return { user };
		},
		{ statusCode: HttpStatus.CREATED },
	);

	private findById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const user = await this.userService.findById(req.params.id);
			return { user };
		},
	);

	private findAll = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			console.log(process.env.DB_DB);
			const users = await this.userService.findAll();
			return { users };
		},
	);

	private updateById = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const user = await this.userService.updateById(
				req.params.id,
				req.body,
			);
			return { user };
		},
	);
}
