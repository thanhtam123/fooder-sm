import catchAsync from '../../utils/catchAsync';
import BaseController from '../../base/base.controller';
import AuthService from './auth.service';
import { NextFunction, Request, Response } from 'express';
import validationMiddleware from '../../middlewares/validation.middleware';
import LoginDto from './dto/login.dto';
import RegisterDto from './dto/register.dto';
import HttpStatus from '../../utils/httpStatusCode';

export default class AuthController extends BaseController {
	private authService: AuthService;

	constructor(authService: AuthService) {
		super('/auth');
		this.authService = authService;

		this.intializeRoutes();
	}

	intializeRoutes() {
		this.router.post('/login', validationMiddleware(LoginDto), this.login);
		this.router.post(
			'/register',
			validationMiddleware(RegisterDto),
			this.register,
		);
	}

	private login = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const { username, password } = req.body;
			const user = await this.authService.login({ username, password });
			return { user };
		},
	);

	private register = catchAsync(
		async (req: Request, res: Response, next: NextFunction) => {
			const user = await this.authService.register(req.body);
			return { user };
		},
		{ statusCode: HttpStatus.CREATED },
	);
}
