import UserInterface from '../../interfaces/user.interface';
import UserEntity from '../../entities/user.entity';
import LoginDto from './dto/login.dto';
import RegisterDto from './dto/register.dto';
import HttpUnauthorizedException from '../../http-exceptions/httpUnauthorized.exception';
import HttpBadRequestException from '../../http-exceptions/httpBadRequest.exception';

export default class AuthService {
	async login(loginBody: LoginDto): Promise<UserInterface> {
		const { username, password } = loginBody;
		const user = await UserEntity.findOne({ username });
		if (!user || !user.comparePassword(password)) {
			throw new HttpUnauthorizedException('Invalid email or password.');
		}
		return user;
	}

	async register(registerBody: RegisterDto): Promise<UserInterface> {
		const { username, email } = registerBody;
		const nameUser = await UserEntity.findOne({ username });
		if (nameUser) {
			throw new HttpBadRequestException(
				`Username '${username}' is already in use.`,
			);
		}
		const emailUser = await UserEntity.findOne({ email });
		if (emailUser) {
			throw new HttpBadRequestException(
				`Email '${email}' is already in use.`,
			);
		}

		const user = UserEntity.create(registerBody);
		await user.save();
		return user;
	}
}
