import * as express from 'express';
import { Application, Request, Response, NextFunction } from 'express';
import { errorConverter, errorHandler } from './middlewares/error.middleware';
import * as cors from 'cors';
import BaseController from './base/base.controller';
import catchAsync from './utils/catchAsync';
import loggerMiddleware from './middlewares/consoleLogger.middleware';
import HttpStatus from './utils/httpStatusCode';
import { createConnection, Connection } from 'typeorm';
import { getOrmConfig } from './config/ormconfig';
import * as mongoose from 'mongoose';
import { getMongoConfig } from './config/mongoConfig';

export default class App {
	public app: Application;
	public port: number;

	constructor(controllers: BaseController[], port?: number) {
		this.app = express();
		this.port = port || Number(process.env.PORT) || 8000;

		this.initializeMiddlewares();
		this.connectToTheDatabase();
		this.connectToMongo();
		this.initializeControllers(controllers);
		this.initializeErrorHandler();
	}

	private initializeMiddlewares() {
		this.app.use(cors());

		// parse application/x-www-form-urlencoded
		this.app.use(express.urlencoded({ extended: true }));

		// parse application/json
		this.app.use(express.json());

		// log request to console
		this.app.use(loggerMiddleware);
	}

	private initializeControllers(controllers: BaseController[]) {
		// the first route
		this.app.get(
			'/',
			catchAsync(
				async (req: Request, res: Response, next: NextFunction) => {
					const message: string = 'Welcome to FooderSM';
					return { message };
				},
				{ statusCode: HttpStatus.OK },
			),
		);
		//

		controllers.forEach((controllers: BaseController) => {
			this.app.use(controllers.path, controllers.router);
		});
	}

	private initializeErrorHandler() {
		this.app.use(errorConverter);
		this.app.use(errorHandler);
	}

	private async connectToTheDatabase(): Promise<Connection> {
		try {
			const connection = await createConnection(getOrmConfig());
			console.log('Successfully connect to MySQL');
			return connection;
		} catch (err) {
			console.log(err);
		}
	}

	private async connectToMongo() {
		mongoose
			.connect(process.env.MONGO_URI, getMongoConfig())
			.then(() => {
				console.log('Successfully connect to MongoDB.');
			})
			.catch((err: Error) => {
				console.error('MongoDB connection error', err);
				process.exit();
			});
	}

	public listen() {
		this.app.listen(this.port, () => {
			console.log(`App listening on the port ${this.port}`);
		});
	}
}
