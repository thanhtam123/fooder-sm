export default interface CRUDBaseInterface {
	create(createBody: object): Promise<any>;
	findById(id: string): Promise<any>;
	findAll(): Promise<any>;
	updateById(id: string, updateBody: object): Promise<any>;
	deleteById(id: string): Promise<any>;
}
