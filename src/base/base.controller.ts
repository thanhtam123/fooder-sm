import { Router } from 'express';

export default abstract class BaseController {
	path: string;
	router: Router;

	constructor(path: string) {
		this.path = path;
		this.router = Router();
	}

	abstract intializeRoutes(): void;
}
