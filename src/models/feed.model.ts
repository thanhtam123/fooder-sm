import FeedInterface from '../interfaces/feed.interface';
import * as mongoose from 'mongoose';

const feedSchema = new mongoose.Schema(
	{
		userId: {
			type: String,
			trim: true,
			required: true,
		},
		content: {
			type: String,
			required: true,
		},
		imgs: {
			type: [String],
			default: [],
		},
	},
	{
		timestamps: true,
	},
);

const FeedModel = mongoose.model<FeedInterface & mongoose.Document>(
	'Feed',
	feedSchema,
);
export default FeedModel;
