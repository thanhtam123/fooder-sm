## Description

This is a project template that consist base code for projects using express, typeorm, mysql and typescript.

## Installation

```bash
$ npm install
```

## Before running the app

You must create a **.env** file in root folder like **.sample.env**, or like this example:

```bash
# api port
PORT=8000

# JWT config
JWT_SECRET=Small-TASK
JWT_EXPIRATION_TIME=3600

# TypeORM config
DB_HOST=localhost
DB_PORT=3306
DB_USER=root
DB_PASSWORD=thanhtam
DB_DB=fooder

# Mongo config
MONGO_URI=mongodb://localhost:27017/fooder
```

## Running the app

```bash
# development
$ npm run dev

# production
$ npm start
```
